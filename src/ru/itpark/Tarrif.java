package ru.itpark;

public class Tarrif {
    private int minute;
    private int traffic;
    private int subscription;

    public Tarrif(int minute, int traffic, int subscription) {
        this.minute = minute;
        this.traffic = traffic;
        this.subscription = subscription;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getTraffic() {
        return traffic;
    }

    public void setTraffic(int traffic) {
        this.traffic = traffic;
    }

    public int getSubscription() {
        return subscription;
    }

    public void setSubscription(int subscription) {
        this.subscription = subscription;
    }

    @Override
    public String toString() {
        return "Tarrif{" +
                "minute=" + minute +
                ", traffic=" + traffic +
                ", subscription=" + subscription +
                '}';
    }
}
